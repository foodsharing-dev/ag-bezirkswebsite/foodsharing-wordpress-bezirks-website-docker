#! /bin/bash
./stop.sh

source docker.config
if ! [ -x "$(command -v $dc_command1)" ]; then
  dc_command=$dc_command2
  else 
  dc_command=$dc_command1
fi

docker-compose rm -v
sudo rm -rf wp-app
