#! /bin/bash
source docker.config

if ! [ -x "$(command -v $dc_command1)" ]; then
  dc_command=$dc_command2
  else 
  dc_command=$dc_command1
fi

$dc_command -f docker-compose.yml down
