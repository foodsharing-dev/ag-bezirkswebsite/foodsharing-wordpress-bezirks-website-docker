#! /bin/bash
source docker.config

if ! [ -x "$(command -v $dc_command1)" ]; then
  dc_command=$dc_command2
  else 
  dc_command=$dc_command1
fi

echo "Run command ... " $dc_command $dc_command_cli option update siteurl $site_url:$wp_port
$dc_command $dc_command_cli option update siteurl $site_url:$wp_port

echo "Run command ... " $dc_command $dc_command_cli option update home $site_url:$wp_port
$dc_command $dc_command_cli option update home $site_url:$wp_port

echo "Run command ... " $dc_command $dc_command_cli search-replace $site_url_now $site_url:$wp_port
$dc_command $dc_command_cli search-replace $site_url_db $site_url:$wp_port

echo "Run command ... " $dc_command $dc_command_cli user update $adminuser --user_email=$adminmail --user_pass=$adminpasswd --role=administrator --skip-email        
$dc_command $dc_command_cli user update $adminuser --user_email=$adminmail --user_pass=$adminpasswd --role=administrator --skip-email

echo "Run command ... " $dc_command $dc_command_cli language core install de_DE
$dc_command $dc_command_cli language core install de_DE

echo "Run command ... " $dc_command $dc_command_cli plugin install the-events-calendar --activate
$dc_command $dc_command_cli plugin install the-events-calendar --activate

echo "Run command ... " $dc_command $dc_command_cli plugin delete akismet
$dc_command $dc_command_cli plugin delete akismet

echo "Run command ... " $dc_command $dc_command_cli plugin delete hello
$dc_command $dc_command_cli plugin delete hello

echo "Run command ... " $dc_command $dc_command_cli core update
$dc_command $dc_command_cli core update

echo "Run command ... " $dc_command $dc_command_cli language plugin update --all
$dc_command $dc_command_cli language plugin update --all

echo "The foodsharing wordpress project is on $site_url:$wp_port available. 
You can look or change something in database on $site_url:$phpmyadmin_port."