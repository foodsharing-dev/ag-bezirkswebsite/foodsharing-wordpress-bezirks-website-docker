#! /bin/bash
source docker.config

if ! [ -x "$(command -v $dc_command1)" ]; then
  dc_command=$dc_command2
  dc_cmd_exec=$dc_cmd_exec2
  else 
  dc_command=$dc_command1
  dc_cmd_exec=$dc_cmd_exec1
fi

echo "Stopping docker"
./stop.sh
echo "Starting docker"
$dc_command -f docker-compose.yml up -d
echo "Run command ... " $chown_uploads
$dc_cmd_exec $dc_command_exec -c "$chown_uploads"
echo "Run command ... " $chmod_uploads
$dc_cmd_exec $dc_command_exec -c "$chmod_uploads"
echo "Run command ... " sudo rm -f -R ../themes/twentynineteen ../themes/twentyseventeen ../themes/twentysixteen
sudo rm -f -R ../themes/twentynineteen ../themes/twentyseventeen ../themes/twentysixteen
echo "Run command ... " $chown_volumes
$chown_volumes 
echo "Run command ... " $chmod_volumes
$chmod_volumes

echo "Start the script ./wp-cli.sh to change siteurl and admin password."